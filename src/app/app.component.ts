import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'erhuoxiubao-root',
  templateUrl: './app.component.html',
})
export class AppComponent {

  constructor(private router: Router) {
  }

  goHome() {
    this.router.navigateByUrl('');
  }

}
