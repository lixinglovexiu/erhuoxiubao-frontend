import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgZorroAntdModule, NZ_I18N, zh_CN } from 'ng-zorro-antd';
import { registerLocaleData } from '@angular/common';
import zh from '@angular/common/locales/zh';
import { PhotoWallComponent } from './photo-wall/photo-wall.component';
import {UploadComponent} from './upload/upload.component';
import {AppRoutingModule} from './app-routing.module';
import { ChoosePhotosComponent } from './photo-wall/choose-photos/choose-photos.component';
import {ButtonComponent} from './button/button.component';
import { Type1Component } from './photo-wall/type1/type1.component';
import { WallListComponent } from './photo-wall/wall-list/wall-list.component';
import { WallAddEditComponent } from './photo-wall/wall-add-edit/wall-add-edit.component';
import { TypeListComponent } from './photo-wall/type-list/type-list.component';
import { MainComponent } from './main.component';

registerLocaleData(zh);

@NgModule({
  declarations: [
    AppComponent,
    PhotoWallComponent,
    UploadComponent,
    ChoosePhotosComponent,
    ButtonComponent,
    Type1Component,
    WallListComponent,
    WallAddEditComponent,
    TypeListComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgZorroAntdModule,
    AppRoutingModule
  ],
  providers: [{ provide: NZ_I18N, useValue: zh_CN }],
  bootstrap: [AppComponent]
})
export class AppModule { }
