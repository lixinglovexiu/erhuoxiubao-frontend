import {Component, EventEmitter, Output} from '@angular/core';
import {UploadFile, NzMessageService} from 'ng-zorro-antd';
import {TengxunyunService} from '../tengxunyun.service';
import {HttpService} from '../http.service';
import {Router} from '@angular/router';

@Component({
  selector: 'erhuoxiubao-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})

export class UploadComponent {
  fileTypes = 'image/png,image/jpeg,image/gif,image/bmp';
  filelist: UploadFile[] = [];
  images = [];
  uploading = false;
  subscription = null;

  @Output() finish_upload = new EventEmitter();

  constructor(
    private tengxunyunService: TengxunyunService,
    private httpService: HttpService,
    private message: NzMessageService,
  ) {
    // 为了初始化tengxunyunService的observer
    this.tengxunyunService.engineStatus.subscribe();
  }

  beforeUpload = (file: UploadFile): boolean => {
    this.filelist.push(file);
    return false;
  };

  // 上传
  tengxunUpload() {
    this.uploading = true;
    let fileListSize = this.filelist.length;
    this.filelist.forEach(file => {
      this.tengxunyunService.uploadFile(file, file.type, file.name);
    });

    this.subscription = this.tengxunyunService.engineStatus.subscribe({
      next: result => {
        const urlObj = this.tengxunyunService.getUrlByFile();
        const imageObj = new ImageObj(urlObj.url, urlObj.desc);
        this.images.push(imageObj);
        // this.save_2_db(urlObj.urlString, urlObj.name);
        fileListSize--;
        if (fileListSize === 0) {
          this.message.success('上传完成');
          this.uploading = false;
          this.filelist = [];
          this.finish_upload.emit(this.images);
          this.images = [];
          this.subscription.unsubscribe();
        }
      }
    });

  }
}

class ImageObj {
  constructor(public url: string, public desc: string) {
  }
}
