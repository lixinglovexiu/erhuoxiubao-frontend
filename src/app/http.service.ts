import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  apiUrl = 'http://localhost:8000';

  constructor(private httpClient: HttpClient) {
  }

  get(url, params) {
    const query_url = this.apiUrl + url;
    params['responseType'] = 'json';
    return this.httpClient.get(query_url, params);
  }

  post(url, params) {
    const post_url = this.apiUrl + url;
    return this.httpClient.post(post_url, params);
  }

  delete(url, params) {
    const post_url = this.apiUrl + url;
    return this.httpClient.delete(post_url, params);
  }

  patch(url, params) {
    const post_url = this.apiUrl + url;
    return this.httpClient.patch(post_url, params);
  }

  check_result(data) {
    const result = data['result'];
    const success: boolean = result['success'];
    const displaymsg = result['displaymsg'];
    if (success) {
      return null;
    } else {
      return displaymsg;
    }
  }

  check_post_body(body) {
    let page_check_pass = true;
    for (const i in body) {
      if (body.hasOwnProperty(i)) {
        body[i].markAsDirty();
        body[i].updateValueAndValidity();
        if (body[i].errors) {
          page_check_pass = false;
        }
      }
    }
    return page_check_pass;
  }
}
