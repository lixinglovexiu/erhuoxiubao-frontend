import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PhotoWallComponent} from './photo-wall/photo-wall.component';
import {Type1Component} from './photo-wall/type1/type1.component';
import {WallListComponent} from './photo-wall/wall-list/wall-list.component';
import {WallAddEditComponent} from './photo-wall/wall-add-edit/wall-add-edit.component';
import {MainComponent} from './main.component';


const routes: Routes = [
  {
    path: '', component: MainComponent,
    children: [
      {path: '', component: WallListComponent},
      {path: 'add', component: WallAddEditComponent},
      {path: 'edit/:id', component: WallAddEditComponent},
      {path: 'type1/:photo_wall_id', component: Type1Component},
    ]
  },
  {path: 'type0/:photo_wall_id', component: PhotoWallComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AppRoutingModule {
}
