import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NzMessageService, NzModalService, UploadFile} from 'ng-zorro-antd';
import {HttpService} from '../../http.service';
import {forEach} from '../../../../node_modules/@angular/router/src/utils/collection';
import {TengxunyunService} from '../../tengxunyun.service';

@Component({
  selector: 'erhuoxiubao-choose-photos',
  templateUrl: './choose-photos.component.html',
  styleUrls: ['./choose-photos.component.css']
})
export class ChoosePhotosComponent implements OnInit {

  @Input() images = [];
  @Output() removeAction = new EventEmitter();

  constructor(
    private message: NzMessageService,
    private modalService: NzModalService,
    private httpService: HttpService,
    private tengxunyunService: TengxunyunService) {
    // 为了初始化tengxunyunService的observer
    this.tengxunyunService.engineStatus.subscribe();
  }

  ngOnInit() {
  }

  loadPhotos() {
    this.httpService.get(
      '/api/v1/photo', {}
    ).subscribe(data => {
      const errMsg = this.httpService.check_result(data);
      if (errMsg != null) {
        this.message.create('error', errMsg);
      }
      this.images = data['data']['photos'];
    });
  }

  remove = (image) => {
    this.tengxunyunService.deleteFile(image.url);
    if (image.id !== undefined) {
      this.deleteFromDb(image.id);
    }
    this.removeAction.emit(image);


  };

  deleteFromDb(id: number) {
    this.httpService.delete(
      `/api/v1/photo/${id}`,
      {}
    ).subscribe(
      data => {
        const error_msg = this.httpService.check_result(data);
        if (error_msg != null) {
          this.message.create('error', error_msg);
        }
      }
    );
  }

}
