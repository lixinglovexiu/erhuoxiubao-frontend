import {Component, OnDestroy, OnInit} from '@angular/core';
import {HttpService} from '../../http.service';
import {NzMessageService} from 'ng-zorro-antd';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'erhuoxiubao-type1',
  templateUrl: './type1.component.html',
  styleUrls: ['./type1.component.css']
})
export class Type1Component implements OnInit, OnDestroy{
  images = [];
  photo_wall_id: number;
  subscription: Subscription;

  constructor(
    private httpService: HttpService,
    private message: NzMessageService,
    private activateRoute: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.subscription = this.activateRoute.params.subscribe(param => {
      this.photo_wall_id = param['photo_wall_id'];
    });
    this.loadPhotos(this.photo_wall_id);
  }


  loadPhotos(photo_wall_id: number) {
    if (photo_wall_id === undefined) {
      return;
    }
    this.httpService.get(
      '/api/v1/photo_wall/' + photo_wall_id + '/photos', {}
    ).subscribe(data => {
      const errMsg = this.httpService.check_result(data);
      if (errMsg != null) {
        this.message.create('error', errMsg);
      }
      const photos = data['data']['photos'];
      this.images = photos;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
