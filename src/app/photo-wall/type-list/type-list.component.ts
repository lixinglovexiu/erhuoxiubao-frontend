import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';

@Component({
  selector: 'erhuoxiubao-type-list',
  templateUrl: './type-list.component.html',
  styleUrls: ['./type-list.component.css']
})
export class TypeListComponent implements OnInit {
  @Output() commitValue = new EventEmitter();
  @Input() radioValue = '0';

  images = [
    {
      id: 0,
      url: 'https://lixing-1257240793.cos.ap-beijing.myqcloud.com/avator/type0.jpg',
      desc: '画廊'
    }, {
      id: 1,
      url: 'https://lixing-1257240793.cos.ap-beijing.myqcloud.com/avator/type1.jpg',
      desc: '蜂窝'
    },
  ];


  constructor() {
  }

  ngOnInit() {
  }

  setValue() {
    this.commitValue.emit(this.radioValue);
  }
}
