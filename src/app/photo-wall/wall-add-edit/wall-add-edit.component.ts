import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpService} from '../../http.service';
import {NzMessageService} from 'ng-zorro-antd';
import {ActivatedRoute, Router} from '@angular/router';
import {isString} from 'util';

@Component({
  selector: 'erhuoxiubao-wall-add-edit',
  templateUrl: './wall-add-edit.component.html',
  styleUrls: ['./wall-add-edit.component.css']
})
export class WallAddEditComponent implements OnInit {

  images = [];
  photoWallName = '';
  photoWallDesc = '';
  photoWallType = '0';
  photoWallId: number;

  validateForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private httpService: HttpService,
    private message: NzMessageService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.params.subscribe(
      params => {
        const photoWallId = params['id'];
        if (photoWallId !== undefined) {
          this.loadPhotoWall(photoWallId);
        }
      }
    );
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      name: [null, [Validators.required]],
      desc: [null, []],
    });
  }

  submitForm(): void {
    const page_check_pass = this.httpService.check_post_body(this.validateForm.controls);
    if (page_check_pass) {
      this.createOrEditPhotoWall(this.photoWallId, this.photoWallName, this.photoWallDesc, this.images, this.photoWallType);
    } else {
      return;
    }
  }

  changeType(photo_wall_type: string) {
    this.photoWallType = photo_wall_type;
  }

  refresh(images) {
    this.images = this.images.concat(images);
  }

  private createOrEditPhotoWall(id, name, desc, images, type) {
    const params = {
      id: id,
      photos: images,
      name: name,
      desc: desc,
      type: type
    };

    if (id === undefined) {
      this.httpService.post(
        '/api/v1/photo_wall',
        params
      ).subscribe(
        data => {
          const error_msg = this.httpService.check_result(data);
          if (error_msg != null) {
            this.message.error(error_msg);
          } else {
            this.router.navigate(['']);
          }
        }
      );
    } else {
      this.httpService.patch(
        '/api/v1/photo_wall/' + id,
        params
      ).subscribe(
        data => {
          const error_msg = this.httpService.check_result(data);
          if (error_msg != null) {
            this.message.error(error_msg);
          } else {
            this.router.navigate(['']);
          }
        }
      );
    }

  }

  return2List() {
    this.router.navigate(['']);
  }

  remove(image) {
    this.images = this.images.filter(function (item) {
      return item !== image;
    });
  }

  loadPhotoWall(photoWallId) {
    this.httpService.get(
      '/api/v1/photo_wall/' + photoWallId,
      {}
    ).subscribe(
      data => {
        const error_msg = this.httpService.check_result(data);
        if (error_msg != null) {
          this.message.error(error_msg);
        } else {
          const photoWallObj = data['data']['photo_wall'];
          this.photoWallId = photoWallObj['id'];
          this.photoWallName = photoWallObj['name'];
          this.photoWallDesc = photoWallObj['desc'];
          this.photoWallType = photoWallObj['type'].toString();
          this.images = photoWallObj['images'];
        }
      }
    );
  }

}
