import {Component, OnDestroy, OnInit} from '@angular/core';
import {HttpService} from '../http.service';
import {NzMessageService} from 'ng-zorro-antd';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';

declare var polaroidGallery: any;

@Component({
  selector: 'erhuoxiubao-photo-wall',
  templateUrl: './photo-wall.component.html',
  styleUrls: ['./photo-wall.component.css']
})
export class PhotoWallComponent implements OnInit, OnDestroy {
  photo_wall_id: number;
  private subscription: Subscription;

  constructor(
    private httpService: HttpService,
    private message: NzMessageService,
    private activateRoute: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.subscription = this.activateRoute.params.subscribe(param => {
      this.photo_wall_id = param['photo_wall_id'];
    });
    this.loadPhotos(this.photo_wall_id);
  }

  loadPhotos(photo_wall_id: number) {
    if (photo_wall_id === undefined) {
      return;
    }
    this.httpService.get(
      '/api/v1/photo_wall/' + photo_wall_id + '/photos', {}
    ).subscribe(data => {
      const errMsg = this.httpService.check_result(data);
      if (errMsg != null) {
        this.message.create('error', errMsg);
      }
      const photos = data['data']['photos'];
      const dataArr = [];
      photos.forEach(photo => {
        const tmpData = {};
        tmpData['name'] = photo['url'];
        tmpData['caption'] = photo['desc'];
        dataArr.push(tmpData);
      });
      polaroidGallery(dataArr);
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
