import {Component, OnInit} from '@angular/core';
import {HttpService} from '../../http.service';
import {NzMessageService} from 'ng-zorro-antd';
import {Router} from '@angular/router';

@Component({
  selector: 'erhuoxiubao-wall-list',
  templateUrl: './wall-list.component.html',
  styleUrls: ['./wall-list.component.css']
})
export class WallListComponent implements OnInit {
  data = [];

  constructor(
    private httpService: HttpService,
    private message: NzMessageService,
    private router: Router) {
  }

  ngOnInit() {
    this.getDatas();
  }

  getDatas() {
    this.httpService.get(
      '/api/v1/photo_wall',
      {}
    ).subscribe(
      data => {
        const error_msg = this.httpService.check_result(data);
        if (error_msg != null) {
          this.message.error(error_msg);
        } else {
          this.data = [];
          const photoWalls = data['data']['photo_walls'];
          photoWalls.forEach(photoWall => {
            const dataObj = new DataObj(
              photoWall.id,
              photoWall.name,
              photoWall.desc,
              photoWall.type);
            this.data.push(dataObj);
          });
          this.data = JSON.parse(JSON.stringify(this.data));
        }
      }
    );
  }

  add() {
    this.router.navigate(['add']);
  }

  edit(id) {
    this.router.navigate(['edit', id]);

  }

  delete(id) {
    this.httpService.delete(
      '/api/v1/photo_wall/' + id,
      {}
    ).subscribe(result => {
        const checkResult = this.httpService.check_result(result);
        if (checkResult != null) {
          this.message.error(checkResult);
        } else {
          this.getDatas();
        }
      }
    );
  }

  detail(type, id) {
    const url = '/type' + type + '/' + id;
    if (type === 1) {
      this.router.navigateByUrl(url);
    } else {
      window.open(url);
    }
  }
}

class DataObj {
  constructor(
    public id: number,
    public title: string,
    public desc: string,
    public type: string) {
  }
}
