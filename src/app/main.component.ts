import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'erhuoxiubao-main',
  templateUrl: './main.component.html',
})
export class MainComponent {

  constructor(private router: Router) {
  }

  goHome() {
    this.router.navigateByUrl('');
  }

}
