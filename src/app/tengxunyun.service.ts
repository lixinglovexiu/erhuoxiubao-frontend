// 腾讯云 COS 的使用
import {Injectable} from '@angular/core';
import 'rxjs/add/operator/share';
import {Observable} from 'rxjs';
import {Observer} from 'rxjs';
import {environment} from '../environments/environment';

import COS from 'cos-js-sdk-v5';

@Injectable({
  providedIn: 'root'
})
export class TengxunyunService {
  engineStatus: Observable<string>;
  private observer: Observer<string>;
  urlObj = {
    url: '',
    desc: ''
  };
  cos = new COS({
    AppId: environment.txyun.appId,
    SecretId: environment.txyun.secretId,
    SecretKey: environment.txyun.secretKey,
  });

  constructor() {
    this.engineStatus = new Observable<string>(observer => this.observer = observer).share();
  }

  guid() {
    const s4 = () =>
      Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    return `${s4()}${s4()}-${s4()}-${s4()}-${s4()}-${s4()}${s4()}${s4()}`;
  }

  getUrlByFile() {
    return this.urlObj;
  }

  uploadFile(file, type, name) {
    const vm = this;
    const storeAs = `${environment.txyun.fileName}/${new Date().getTime()}-${this.guid()}-${name}`;
    // this.cos.sliceUploadFile({
    this.cos.putObject({
      Bucket: environment.txyun.bucket,
      Region: environment.txyun.region,
      Key: storeAs,
      Body: file
    }, function (err, data) {
      if (err) {
        console.error(err);
        vm.observer.next('uploadFalse');
      } else {
        vm.urlObj.url = `${environment.txyun.urlStr}/` + storeAs;
        vm.urlObj.desc = name;
        vm.observer.next('uploadSuccess');
      }
    });
  }

  deleteFile(filePath) {
    const vm = this;
    const key = filePath.substring(filePath.indexOf(`${environment.txyun.fileName}`));
    const params = {
      Bucket: environment.txyun.bucket,
      Region: environment.txyun.region,
      Key: key,
    };
    this.cos.deleteObject(params, function (err, data) {
      if (err) {
        console.error('error:' + err);
        vm.observer.next('deleteFalse');
      } else {
        vm.observer.next('deleteSuccess');
      }
    });
  }
}
